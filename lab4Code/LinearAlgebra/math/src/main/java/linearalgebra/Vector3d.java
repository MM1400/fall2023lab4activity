// Marin Melentii 2234078
package linearalgebra;
public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public double getX() {
        return this.x;
    }
    public double getY() {
        return this.y;
    }
    public double getZ() {
        return this.z;
    }
    public double magnitude() {
        return (Math.sqrt(x*x+y*y+z*z));
    }
    public double dotProduct(Vector3d vector) {
        return (vector.getX()*this.x) + (vector.getY()*this.y) + 
        (vector.getZ()*this.z);
    }
   public Vector3d add(Vector3d vector) {
        Vector3d v = new Vector3d(this.x+ vector.getX(), this.y + vector.getY(), 
        this.z + vector.getZ());
        return v;
   }
}