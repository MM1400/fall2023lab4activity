// marin melentii 2234078
package linearalgebra;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
public class Vector3dTests {
    
    @Test
    public void getX_returns1() {
        Vector3d v = new Vector3d(1,2,3);
        assertEquals(1, v.getX(), 0.01);
    }
    @Test
    public void getY_returns2() {
        Vector3d v = new Vector3d(1,2,3);
        assertEquals(2, v.getY(), 0.01);
    }
    @Test
    public void getZ_returns3() {
        Vector3d v = new Vector3d(1,2,3);
        assertEquals(3, v.getZ(), 0.01);
    }
    @Test
    public void magnitude_returnsSqrt6() {
        Vector3d v = new Vector3d(1,1,2);
        assertEquals(Math.sqrt(6), v.magnitude(), 0);
    }
    @Test
    public void dotProduct_returns13() {
        Vector3d v1 = new Vector3d(1,1,2);
        Vector3d v2 = new Vector3d(2,3,4);
        assertEquals(13,v1.dotProduct(v2), 0);
    }
    @Test
    public void add_returns346() {
        Vector3d v1 = new Vector3d(1,1,2);
        Vector3d v2 = new Vector3d(2,3,4);
        Vector3d v3 = v1.add(v2);
        assertEquals(3, v3.getX(), 0);
        assertEquals(4, v3.getY(), 0);
        assertEquals(6, v3.getZ(), 0);
    }
}
